#include"definition.h"

typedef struct 
{
	uint8_t *data_t;
	uint8_t	tail;
	uint8_t head;
	uint8_t isFull;
	
}Circular_buf;

void init(Circular_buf *list, uint8_t *arr);

int insert(Circular_buf *list, uint8_t num);
