#include"circular.h"

void init(Circular_buf *list, uint8_t *arr)
{
	list->data_t = arr;
	list->head = 0;
	list->tail = 0;
	list->isFull = 1;
}

int insert(Circular_buf *list, uint8_t num)
{
	if(list->isFull)
	{
		list->data_t[list->tail] = num;		
		list->tail++;
		if(list->tail == MAX_SIZE)
		{
			list->tail = 0;
		}
	}
	if(list->tail == list->head)		
		list->isFull = 0;	
	//printf("F = %d", list->tail);
	return list->isFull;
}

